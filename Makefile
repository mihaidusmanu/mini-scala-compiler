CMO=lexer.cmo parser.cmo typing.cmo x86_64.cmo compile.cmo main.cmo 
GENERATED=lexer.ml parser.ml parser.mli 
BIN=pscala
FLAGS=-annot -g

all: $(BIN)
	./$(BIN) test.scala
	gcc test.s -o test.out

.PHONY: tests

tests: $(BIN)
	for f in tests/*.scala; do ./$(BIN) $$f; done

$(BIN): $(CMO)
	ocamlc $(FLAGS) -o $(BIN) $(CMO)

.SUFFIXES: .mli .ml .cmi .cmo .mll .mly

.mli.cmi:
	ocamlc $(FLAGS) -c  $<

.ml.cmo:
	ocamlc $(FLAGS) -c $<

.mll.ml:
	ocamllex $<

.mly.ml:
	menhir -v $<

.mly.mli:
	menhir -v $<

clean:
	rm -f *.cm[io] *.o *.annot *~ pscala $(GENERATED)
	rm -f *.out *.s
	rm -f parser.output parser.automaton parser.conflicts

.depend depend:$(GENERATED)
	rm -f .depend
	ocamldep *.ml *.mli > .depend

include .depend



