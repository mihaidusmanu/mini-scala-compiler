%{
    open Ast
    open Lexing

    let pos (p : Lexing.position) (p' : Lexing.position) = (p.pos_lnum, p.pos_cnum - p.pos_bol + 1, p'.pos_cnum - p.pos_bol + 1) 
%}
%token Tclass Tdef Telse Teq Textends Tif 
%token Tne Tnew Tnull Tobject Toverride Tmain
%token Tprint Treturn Tthis Tval Tvar Twhile 
%token Teof Tlbracket Trbracket Tcomma Tcolon 
%token Tscolon Tlrobracket Trrobracket Tlsqbracket Trsqbracket
%token Tassign Tor Tand Teqeq Tdiff Tg Tge Tl Tle 
%token Tplus Tminus Tmult Tmod Tdiv Tnot Tpoint Tsclass Toclass
%token <string> Tident
%token <int> Tpint Tnint 
%token <bool> Tbool 
%token <string> Tstring

%nonassoc Tif
%nonassoc Telse
%nonassoc Twhile Treturn
%right Tassign
%left Tor
%left Tand
%left Teq Tne Teqeq Tdiff
%left Tg Tge Tl Tle
%left Tplus Tminus Tnint
%left Tmult Tdiv Tmod
%right Tneg Tnot
%left Tpoint


%start prog

%type <Ast.file> prog

%%
prog:
| clist = cls* main = cls_main Teof                                                             { (clist, main) : Ast.file }

cls:
| Tclass id = Tident 
  ptc = loption(delimited(Tlsqbracket, separated_nonempty_list(Tcomma, param_type_class), Trsqbracket))
  par = loption(delimited(Tlrobracket, separated_list(Tcomma, param), Trrobracket))
  ext = preceded(Textends, pair(types, loption(delimited(Tlrobracket, separated_list(Tcomma, expr), Trrobracket))))?
  d = delimited(Tlbracket, separated_list(Tscolon, decl), Trbracket)                            { { cdesc = Cclass (id, ptc, par, ext, d); 
                                                                                                    cloc = (pos $startpos $endpos) } }

decl:
| v = var                                                                                       { v }
| m = meth                                                                                      { m }

var:
| Tvar id = Tident tp = preceded(Tcolon, types)? Tassign e = expr                               { { edesc = Vva (Var, id, tp, e); 
                                                                                                    eloc = (pos $startpos $endpos) } }
| Tval id = Tident tp = preceded(Tcolon, types)? Tassign e = expr                               { { edesc = Vva (Val, id, tp, e); 
                                                                                                    eloc = (pos $startpos $endpos) } }

meth:
| oride = boption(Toverride) Tdef id = Tident
  pt = loption(delimited(Tlsqbracket, separated_nonempty_list(Tcomma, param_type), Trsqbracket))
  par = delimited(Tlrobracket, separated_list(Tcomma, param), Trrobracket)
  bl = block                                                                                    { { edesc = Mdef (oride, id, pt, par, Type ("Unit", []), bl); 
                                                                                                    eloc = (pos $startpos $endpos) } }
| oride = boption(Toverride) Tdef id = Tident
  pt = loption(delimited(Tlsqbracket, separated_nonempty_list(Tcomma, param_type), Trsqbracket))
  par = delimited(Tlrobracket, separated_list(Tcomma, param), Trrobracket)
  tp = preceded(Tcolon, types) Tassign e = expr                                                 { { edesc = Mdef (oride, id, pt, par, tp, e);
                                                                                                    eloc = (pos $startpos $endpos) } }

param:
| id = Tident Tcolon tp = types                                                                 { (id, tp) : Ast.param }

param_type:
| id = Tident tp = preceded(Tsclass, types)                                                     { (id, Some (Lesser, tp)) : Ast.param_type }
| id = Tident tp = preceded(Toclass, types)                                                     { (id, Some (Greater, tp)) : Ast.param_type }
| id = Tident                                                                                   { (id, None) : Ast.param_type }

param_type_class:
| Tplus pt = param_type                                                                         { (Incr, pt) : Ast.param_type_class }
| Tminus pt = param_type                                                                        { (Decr, pt) : Ast.param_type_class }
| pt = param_type                                                                               { (Neut, pt) : Ast.param_type_class }

types:
| id = Tident at = arguments_type                                                               { Type (id, at) }

arguments_type:
| tp = loption(delimited(Tlsqbracket, separated_nonempty_list(Tcomma, types), Trsqbracket))     { tp }

cls_main:
| Tobject Tmain 
  d = delimited(Tlbracket, separated_list(Tscolon, decl), Trbracket)                            { { odesc = Omain d; 
                                                                                                    oloc = (pos $startpos $endpos) } }

expr:
| a = access                                                                                    { { edesc = Eaccess a; 
                                                                                                    eloc = (pos $startpos $endpos) } } 
| a = access at = arguments_type
  e = delimited(Tlrobracket, separated_list(Tcomma, expr), Trrobracket)                         { { edesc = Ecall (a, at, e); 
                                                                                                    eloc = (pos $startpos $endpos) } }
| i = Tnint                                                                                     { { edesc = Ecst (Cint i); 
                                                                                                    eloc = (pos $startpos $endpos) } }
| i = Tpint                                                                                     { { edesc = Ecst (Cint i); 
                                                                                                    eloc = (pos $startpos $endpos) } }
| s = Tstring                                                                                   { { edesc = Ecst (Cstring s); 
                                                                                                    eloc = (pos $startpos $endpos) } }
| b = Tbool                                                                                     { { edesc = Ecst (Cbool b); 
                                                                                                    eloc = (pos $startpos $endpos) } }
| Tlrobracket Trrobracket                                                                       { { edesc = Eunit; 
                                                                                                    eloc = (pos $startpos $endpos) } }
| Tnull                                                                                         { { edesc = Ecst (Cnull); 
                                                                                                    eloc = (pos $startpos $endpos) } }
| Tthis                                                                                         { { edesc = Ethis; 
                                                                                                    eloc = (pos $startpos $endpos) } }
| Tlrobracket e = expr Trrobracket                                                              { e }
| e1 = expr i = Tnint                                                                           { 
                                                                                                    { edesc = Ebinop (Badd, e1, 
                                                                                                        { edesc = Ecst (Cint i); eloc = (pos $startpos $endpos) }); 
                                                                                                      eloc = (pos $startpos $endpos) } 
                                                                                                } 
| e1 = expr o = oper e2 = expr                                                                  { { edesc = Ebinop (o, e1, e2); 
                                                                                                    eloc = (pos $startpos $endpos) } }
| Tnot e = expr                                                                                 { { edesc = Eunop (Unot, e); 
                                                                                                    eloc = (pos $startpos $endpos) } }
| Tminus e = expr                                                                               { { edesc = Eunop (Uneg, e); 
                                                                                                    eloc = (pos $startpos $endpos) } } %prec Tneg
| Tif Tlrobracket e1 = expr Trrobracket e2 = expr                                               { { edesc = Eif (e1, e2, 
                                                                                                    { edesc = Eunit; eloc = e2.eloc }); 
                                                                                                    eloc = (pos $startpos $endpos) } } %prec Tif
| Tif Tlrobracket e1 = expr Trrobracket e2 = expr e3 = preceded(Telse, expr)                    { { edesc = Eif (e1, e2, e3); 
                                                                                                    eloc = (pos $startpos $endpos) } } %prec Telse
| a = access Tassign e = expr                                                                   { { edesc = Eassign (a, e); 
                                                                                                    eloc = (pos $startpos $endpos) } }
| Tnew id = Tident at = arguments_type
  e = delimited(Tlrobracket, separated_list(Tcomma, expr), Trrobracket)                         { { edesc = Enew (id, at, e); 
                                                                                                    eloc = (pos $startpos $endpos) } }
| Twhile Tlrobracket e1 = expr Trrobracket e2 = expr                                            { { edesc = Ewhile (e1, e2); 
                                                                                                    eloc = (pos $startpos $endpos) } } %prec Twhile
| Treturn e = expr                                                                              { { edesc = Ereturn (Some e); 
                                                                                                    eloc = (pos $startpos $endpos) } } 
| Treturn                                                                                       { { edesc = Ereturn (None); 
                                                                                                    eloc = (pos $startpos $endpos) } }
| Tprint Tlrobracket e = expr Trrobracket                                                       { { edesc = Eprint e; 
                                                                                                    eloc = (pos $startpos $endpos) } }
| b = block                                                                                     { b }

blockoper: 
| v = var                                                                                       { v }
| e = expr                                                                                      { e }

block:
| Tlbracket e = separated_list(Tscolon, blockoper) Trbracket                                    { { edesc = Block e; 
                                                                                                    eloc = (pos $startpos $endpos) } }

%inline oper:
| Teq                                                                                           { Beq }
| Tne                                                                                           { Bne }
| Teqeq                                                                                         { Beqeq }
| Tdiff                                                                                         { Bdiff }
| Tl                                                                                            { Bl }
| Tle                                                                                           { Ble }
| Tg                                                                                            { Bg }
| Tge                                                                                           { Bge }
| Tplus                                                                                         { Badd }
| Tminus                                                                                        { Bsub }
| Tmult                                                                                         { Bmul }
| Tdiv                                                                                          { Bdiv }
| Tmod                                                                                          { Bmod }
| Tand                                                                                          { Band }
| Tor                                                                                           { Bor }

access:
| e = expr Tpoint id = Tident                                                                   { (Some e, id) }
| id = Tident                                                                                   { (None, id) }
