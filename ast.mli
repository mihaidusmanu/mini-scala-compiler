type unop = 
    | Uneg | Unot

type binop =
    | Badd | Bsub | Bmul | Bmod | Bdiv
    | Beq | Bne | Beqeq | Bdiff 
    | Bg | Bge | Bl | Ble
    | Bor | Band

type constant =
    | Cnull
    | Cbool of bool
    | Cstring of string
    | Cint of int

type va_ = 
    | Var | Val

type tp = 
    | Type of string * arguments_type

and arguments_type = (tp list)

type param = string * tp

type order = 
    | Greater
    | Lesser

type param_type = string * ((order * tp) option)

type monotony = 
    | Incr
    | Decr
    | Neut
    | No

type param_type_class = monotony * param_type

type expr = {
    edesc : expr_desc;
    eloc : int * int * int
} and expr_desc = 
(* expr *)
    | Eunit
    | Ecst of constant
    | Ethis
    | Eaccess of access
    | Eassign of access * expr
    | Ecall of access * arguments_type * (expr list)
    | Enew of string * arguments_type * (expr list)
    | Eunop of unop * expr
    | Ebinop of binop * expr * expr
    | Eif of expr * expr * expr
    | Ewhile of expr * expr
    | Ereturn of (expr option)
    | Eprint of expr
(* var *)
    | Vva of va_ * string * (tp option) * expr
(* method *)
    | Mdef of bool (* override *) * string * (param_type list) * (param list) * tp * expr
(* block *)
    | Block of (expr list)

and cls = {
    cdesc : cls_desc;
    cloc : int * int * int
} and cls_desc =
    | Cclass of string * (param_type_class list) * (param list) * ((tp * (expr list)) option) * (expr list) (* only var and method *)

and obj = {
    odesc : obj_desc;
    oloc : int * int * int
} and obj_desc =
    | Omain of (expr list) (* only var and method *)

and access = (expr option) * string

type file = (cls list) * obj
