{
    open Parser
    open Lexing

    exception Lexing_error of string

    let newline lexbuf = (
        let pos = lexbuf.lex_curr_p in
            lexbuf.lex_curr_p <- { pos with pos_lnum = pos.pos_lnum + 1; pos_bol = pos.pos_cnum } )
}

let digit = ['0'-'9']
let digitnz = ['1'-'9']
let letter = ['a'-'z' 'A'-'Z']
let character = [' ' '!' '#'-'[' ']'-'~'] 

rule token = parse
| [' ' '\t']+                               { token lexbuf }
| '\n'                                      { newline lexbuf; token lexbuf }
| "/*"                                      { mlcomment lexbuf }
| "//"                                      { slcomment lexbuf }
| "class"                                   { Tclass }
| "def"                                     { Tdef }
| "else"                                    { Telse }
| "eq"                                      { Teq }
| "extends"                                 { Textends }
| "false"                                   { Tbool false }
| "if"                                      { Tif }
| "ne"                                      { Tne }
| "new"                                     { Tnew }
| "null"                                    { Tnull }
| "object"                                  { Tobject }
| "override"                                { Toverride }
| "print"                                   { Tprint }
| "return"                                  { Treturn }
| "this"                                    { Tthis }
| "true"                                    { Tbool true }
| "val"                                     { Tval }
| "var"                                     { Tvar }
| "while"                                   { Twhile }
| "{"                                       { Tlbracket }
| "}"                                       { Trbracket }
| ","                                       { Tcomma }
| "<:"                                      { Tsclass }
| ">:"                                      { Toclass }
| ":"                                       { Tcolon }
| ";"                                       { Tscolon }
| "||"                                      { Tor }
| "&&"                                      { Tand }
| "=="                                      { Teqeq }
| "!="                                      { Tdiff }
| ">"                                       { Tg }
| ">="                                      { Tge }
| "<"                                       { Tl }
| "<="                                      { Tle }
| "="                                       { Tassign }
| "+"                                       { Tplus }
| "-"                                       { Tminus }
| "*"                                       { Tmult }
| "%"                                       { Tmod }
| "/"                                       { Tdiv }
| "!"                                       { Tnot }
| "."                                       { Tpoint }
| "("                                       { Tlrobracket }
| ")"                                       { Trrobracket }
| "["                                       { Tlsqbracket }
| "]"                                       { Trsqbracket }
| "Main"                                    { Tmain }
| '"'                                       { stringp (Buffer.create 1024) lexbuf } 
| '0'                                       { Tpint 0 }
| '-' digitnz digit* as s                   {
                                                try
                                                    let x = int_of_string s in
                                                        if x < -(1 lsl 31) then
                                                            raise (Failure "int_of_string")
                                                        else
                                                            Tnint x
                                                with
                                                | Failure "int_of_string" -> raise (Lexing_error ("Constant too large: " ^ s))
                                            }
| digitnz digit* as s                       { 
                                                try 
                                                    let x = int_of_string s in 
                                                        if x > (1 lsl 31) - 1 then
                                                            raise (Failure "int_of_string")
                                                        else
                                                            Tpint x 
                                                with 
                                                | Failure "int_of_string" -> raise (Lexing_error ("Constant too large: " ^ s)) 
                                            }
| letter (letter | digit | '_')* as s       { Tident s }
| _ as c                                    { raise (Lexing_error ("Illegal character: " ^ String.make 1 c)) }
| eof                                       { Teof }

and mlcomment = parse
| "*/"                                      { token lexbuf }
| "\n"                                      { newline lexbuf; mlcomment lexbuf }
| _                                         { mlcomment lexbuf }
| eof                                       { raise (Lexing_error "Unfinished multi-line comment.") }

and slcomment = parse
| "\n"                                      { newline lexbuf; token lexbuf }
| _                                         { slcomment lexbuf }

and stringp buf = parse
| '"'                                       { Tstring (Buffer.contents buf) }
| '\\' '\\'                                 { Buffer.add_char buf '\\'; stringp buf lexbuf }
| '\\' 'n'                                  { Buffer.add_char buf '\n'; stringp buf lexbuf }
| '\\' '"'                                  { Buffer.add_char buf '\"'; stringp buf lexbuf }
| '\\' 't'                                  { Buffer.add_char buf '\t'; stringp buf lexbuf } 
| character as c                            { Buffer.add_char buf c; stringp buf lexbuf }
| _ as c                                    { raise (Lexing_error ("Illegal character in string." ^ String.make 1 c)) }
| eof                                       { raise (Lexing_error "Unfinished string.") }

{
}
