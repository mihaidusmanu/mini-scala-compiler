open Ast
open Tast

exception Error of (int * int * int) * string

let cNothing = { lbl = Nothing; fields = []; input = []; ext = None}
let cNull = { lbl = Null; fields = []; input = []; ext = None}
let cUnit = { lbl = Unit; fields = []; input = []; ext = None}
let cInt = { lbl = Int; fields = []; input = []; ext = None}
let cBoolean = { lbl = Boolean; fields = []; input = []; ext = None}
let cString = { lbl = String; fields = []; input = []; ext = None}
let cAnyVal = { lbl = AnyVal; fields = []; input = []; ext = None}
let cAnyRef = { lbl = AnyRef; fields = []; input = []; ext = None}
let cAny = { lbl = Any; fields = []; input = []; ext = None}
let tNothing = TClass (("Nothing", Lno), [])
let tNull = TClass (("Null", Lno), [])
let tUnit = TClass (("Unit", Lno), [])
let tInt = TClass (("Int", Lno), [])
let tBoolean = TClass (("Boolean", Lno), [])
let tString = TClass (("String", Lno), [])
let tAnyVal = TClass (("AnyVal", Lno), [])
let tAnyRef = TClass (("AnyRef", Lno), [])
let tAny = TClass (("Any", Lno), [])

let rec str_of_did d = 
    match d with
    | (id, Lno) -> id
    | (id, Lclass id') -> id ^ " of class " ^ id'
    | (id, Lmethod (id', l')) -> id ^ " of method (" ^ str_of_did (id', l') ^ ")"

let str_of_cls c = 
    match c with
    | Nothing -> "Nothing"
    | Null -> "Null"
    | Unit -> "Unit"
    | Int -> "Int"
    | Boolean -> "Boolean"
    | String -> "String"
    | AnyVal -> "AnyVal"
    | AnyRef -> "AnyRef"
    | Any -> "Any"
    | Class (id, _) -> id
    | Dummy (_, d, _) -> str_of_did d

let rec str_of_type t = 
    match t with
    | TClass (cl, tl) -> if tl <> [] then str_of_did cl ^ "[" ^ str_of_type_list tl ^ "]" else str_of_did cl

and str_of_type_list l =
    match l with
    | [] -> ""
    | [x] -> str_of_type x
    | x :: l' -> str_of_type x ^ "," ^ str_of_type_list l'

let get_class_from_env_by_ident env id loc =
    let rec go_through l = (
        match l with
        | [] -> raise (Error (loc, "Class " ^ str_of_did id ^ " is not declared."))
        | x :: l' ->
            if str_of_cls x.lbl = str_of_did id then
                x
            else
                 go_through l'
     ) in go_through env.cls

let get_class_from_env_by_sident env id loc =
    let rec go_through l = (
        match l with
        | [] -> raise (Error (loc, "Class " ^ id ^ " is not declared."))
        | x :: l' -> (
            match x.lbl with
            | Class (id', _) -> if id' = id then x else go_through l'
            | Dummy (_, (id', _), _) -> if id' = id then x else go_through l'
            | _ -> if str_of_cls x.lbl = id then x else go_through l'
        ) 
    ) in go_through env.cls 

let ident_from_type t =
    match t with
    | TClass (id, _) -> id

let str_of_monotony m =
    match m with
    | Incr -> "Positive"
    | Decr -> "Negative"
    | Neut -> "Neutral"
    | No -> assert false 

let opposite m = 
    match m with
    | Incr -> Decr
    | Decr -> Incr
    | Neut -> Neut
    | No -> No

let mon_from_ident env id loc =
    let cl = get_class_from_env_by_ident env id loc in
        match cl.lbl with
        | Dummy (m, _, _) -> m
        | _ -> assert false

let exp_variance env dum expected loc =
    match expected with
    | Incr -> mon_from_ident env dum loc
    | Decr -> opposite (mon_from_ident env dum loc)
    | Neut -> Neut
    | No -> No

let rec check_variance env typ expected loc =
    let TClass (cid, tyl) = typ in
    let cl = get_class_from_env_by_ident env cid loc in
    let rec go_through l1 l2 = (
        match l1, l2 with
        | [], [] -> true
        | x :: l1, y :: l2 -> check_variance env y (exp_variance env x expected loc) loc
        | _ -> assert false
    ) in
        match cl.lbl with
        | Dummy (mon, _, _) -> (mon = No) || (mon = Neut) || (mon = expected)
        | Class (id, dl) -> go_through dl tyl
        | _ -> true

let test_variance env typ expected loc =
    if check_variance env typ expected loc then
        ()
    else
        raise (Error (loc, str_of_type typ ^ " was supposed to be " ^ str_of_monotony expected ^ "."))

let rec subst_from_dummies env dummy dummies sigma loc =
    match dummies, sigma with
    | d :: dummies', s :: sigma' -> if ident_from_type dummy = d then s else subst_from_dummies env dummy dummies' sigma' loc
    | _ -> dummy

let rec subst env ty dummies sigma loc =
    match ty with
    | TClass (cid, tyl) -> ( let cl = get_class_from_env_by_ident env cid loc in
        match cl.lbl with
        | Dummy _ -> subst_from_dummies env (TClass (cid, [])) dummies sigma loc
        | _ -> TClass (cid, List.map (fun t -> subst env t dummies sigma loc) tyl)
    )

let subst_list env tyl dummies sigma loc =
    List.fold_left (fun l ty -> (subst env ty dummies sigma loc) :: l) [] (List.rev tyl)

let get_restr_from_env_by_ident env cl = 
    let rec go_through l = (
        match l with
        | (x, t) :: l' -> if x = cl then t :: go_through l' else go_through l'
        | [] -> []
    ) in 
        go_through env.restr

let dummy_list cl = 
    match cl.lbl with
    | Class (_, dl) -> dl
    | _ -> []

let rec lesser_or_equal env t1 t2 loc = 
    match t1, t2 with
    | TClass (("Nothing", Lno), _), _ -> true
    | TClass (("Null", Lno), _), TClass (cid1, _) -> ( let c1 = get_class_from_env_by_ident env cid1 loc in
        match c1.lbl with
        | Null | String | Class _ | Dummy _ | AnyRef | Any -> true
        | _ -> false
    )
    | TClass (cid1, l1), TClass (cid2, l2) -> ( let c1, c2 = get_class_from_env_by_ident env cid1 loc, get_class_from_env_by_ident env cid2 loc in
        if c1 = c2 then
            lesser_or_equal_same_class env c1 l1 l2 loc
        else (
            let rec restriction_test l = ( 
                match l with
                | [] -> false
                | res :: l' -> if lesser_or_equal env t1 res loc then true else restriction_test l'
            ) in
                match c1.ext with
                | None -> restriction_test (get_restr_from_env_by_ident env cid2) 
                | Some t1 -> (lesser_or_equal env (subst env t1 (dummy_list c1) l1 loc) t2 loc) || (restriction_test (get_restr_from_env_by_ident env cid2))
        )
    )

and lesser_or_equal_same_class env cl l1 l2 loc = 
    let rec go_through dl l1 l2 = (
        match dl, l1, l2 with
        | [], [], [] -> true
        | d :: dl', x1:: l1', x2 :: l2' -> ( let cld = get_class_from_env_by_ident env d loc in
            match cld with
            | { lbl = Dummy (Incr, _, _) } -> lesser_or_equal env x1 x2 loc && go_through dl' l1' l2'
            | { lbl = Dummy (Decr, _, _) } -> lesser_or_equal env x2 x1 loc && go_through dl' l1' l2'
            | { lbl = Dummy (Neut, _, _) } -> equal env x1 x2 loc && go_through dl' l1' l2'
            | _ -> assert false
        )
        | _ -> assert false
    ) in
        go_through (dummy_list cl) l1 l2

and equal env t1 t2 loc = (t1 = t2)

let rec well_formed env t loc = 
    let rec check_bounds env dl tl = (
        match dl, tl with
        | d :: dl', ty :: tl' -> ( let cld = get_class_from_env_by_ident env d loc in
            match cld with
            | { lbl = Dummy (_, _, No) } -> check_bounds env dl' tl'
            | { lbl = Dummy (_, _, Greater t) } -> lesser_or_equal env t ty loc && check_bounds env dl' tl'
            | { lbl = Dummy (_, _, Lesser t) } -> lesser_or_equal env ty t loc && check_bounds env dl' tl'
            | _ -> assert false
        )
        | [], [] -> true
        | _ -> assert false
    ) in
        match t with
        | TClass (cid, tl) -> let cl = get_class_from_env_by_ident env cid loc in
            (List.fold_left (fun b t -> b && (well_formed env t loc)) true tl) && (check_bounds env (dummy_list cl) tl)

let type_from_field f =
    match f with
    | Var t -> t
    | Val t -> t
    | Method (_,  _, t) -> t
    | _ -> assert false

let get_va_info_from_env_by_ident env id loc =
    let rec go_through l = (
        match l with
        | [] -> raise (Error (loc, id ^ " is not defined."))
        | ((x, l), f) :: l' -> 
            if x = id then
                (l, f)
            else 
                go_through l'
    ) in
        go_through env.va

let get_va_type_from_env_by_ident env id loc = 
    let _, f = get_va_info_from_env_by_ident env id loc in type_from_field f

let rec get_field_from_fields_by_ident t l id loc =
    match l with
    | [] -> raise (Error (loc, id ^ " is not a field of " ^ str_of_type t ^ "."))
    | (fid, b, f) :: l' -> 
        if fid = id then
            (b, f)
        else
            get_field_from_fields_by_ident t l' id loc

let get_field_info_from_type_by_ident env t id loc = 
    match t with 
    | TClass (cid, tl) -> (
        let c = get_class_from_env_by_ident env cid loc in
        let b, f = get_field_from_fields_by_ident t c.fields id loc in
        let dl = dummy_list c in
            match f with
            | Var t -> b, Var (subst env t dl tl loc)
            | Val t -> b, Val (subst env t dl tl loc)
            | Method (cll, tyl, t) -> b, Method (cll, subst_list env tyl dl tl loc, subst env t dl tl loc)
            | Input t -> b, Val (subst env t dl tl loc)
    )

let get_field_type_from_type_by_ident env t id loc =
    let _, f = get_field_info_from_type_by_ident env t id loc in type_from_field f

let error_incompatible_types loc t1 str t2 = 
    raise (Error (loc, "Type " ^ str_of_type t1 ^ " doesn't respect the contraints. It was expected to be of type " ^ str ^ str_of_type t2 ^ "."))

let binop_st_typing env bop e1 e1' e2 e2' t tr loc =
    if equal env e1'.typ t loc then (
        if equal env e2'.typ t loc then
            { desc = Ebinop (bop, e1', e2'); typ = tr }
        else
            error_incompatible_types e2.eloc e2'.typ "" t
    ) else
        error_incompatible_types e1.eloc e1'.typ "" t

let max env t1 t2 loc =
    if lesser_or_equal env t1 t2 loc then
        t2
    else if lesser_or_equal env t2 t1 loc then
        t1
    else
        raise (Error (loc, str_of_type t1 ^ " and " ^ str_of_type t2 ^ " are not comparable."))

let req_types_by_class c =
    match c with
    | Nothing | Null | Unit | Int | Boolean | String | AnyVal | AnyRef | Any -> 0
    | Class (_, cl) -> List.length cl
    | Dummy _ -> 0

let rec type_of_tp env tp loc = 
    let Type (id, tpl) = tp in 
    let cl = get_class_from_env_by_sident env id loc in
        if List.length tpl <> req_types_by_class cl.lbl then
            raise (Error (loc, "Type cannot be determined with the given parameters."))
        else (
            match cl.lbl with
            | Class (id', _) -> TClass ((id', Lno), List.fold_left (fun l x -> (type_of_tp env x loc) :: l) [] (List.rev tpl))
            | Dummy (_, id', _) -> TClass (id', [])
            | _ -> TClass ((id, Lno), [])
        )

let type_list_of_tpl env tpl loc =
    List.rev (List.fold_left (fun l tp -> (type_of_tp env tp loc :: l)) [] tpl)

let add_va_to_env env (v : va_) sid t loc = 
    let b = List.fold_left (fun b x -> ((Pervasives.fst x) = sid) || b) false env.va in
        if b then
            raise (Error (loc, "Varible " ^ (Pervasives.fst sid) ^ " is already defined."))
        else (
            match v with
            | Var -> { cls = env.cls; restr = env.restr; va = ((sid, Var t) :: env.va) }
            | Val -> { cls = env.cls; restr = env.restr; va = ((sid, Val t) :: env.va) }
        )

let get_class_from_type env ty loc = get_class_from_env_by_ident env (ident_from_type ty) loc

let get_field_name_from_type_by_ident env ty x loc =
    let cc = ident_from_type ty in
    let (cid, _) = cc in
    let cl = get_class_from_env_by_ident env cc loc in
    let rec go_through l = (
        match l with
        | (id, _, Input _) :: l' -> if (x ^ "$" ^ cid = id) then id else go_through l'
        | (id, _, Var _) :: l' -> if (x = id) then id else go_through l'
        | (id, _, Val _) :: l' -> if (x = id) then id else go_through l'
        | _ :: l' -> go_through l'
        | [] -> raise (Error (loc, x ^ " is not a field of " ^ (str_of_type ty) ^ "."))
    ) in go_through cl.fields

let get_method_from_class_by_ident cl id loc = 
     let rec go_through l = (
        match l with
        | [] -> raise (Error (loc, "Method " ^ id ^ " is not defined."))
        | (id', _, f) :: l' ->
            if id' = id then 
                match f with
                | Var _ | Val _ -> raise (Error (loc, id ^ " is not a method."))
                | Method (cll, tyl, t) -> (cll, tyl, t)
                | _ -> assert false
            else
                 go_through l'
    ) in go_through cl.fields

let get_perm_from_type t =
    match t with
    | TClass (_, perm) -> perm

let rec expr_typing env depth (expr : Ast.expr_desc) loc tr =
    match expr with
    | Eunit -> { desc = Eunit; typ = tUnit }
    | Ecst c -> (
        match c with
        | Cnull -> { desc = Ecst c; typ =  tNull }
        | Cbool _ -> { desc = Ecst c; typ = tBoolean }
        | Cstring _ -> { desc = Ecst c; typ = tString } 
        | Cint _ -> { desc = Ecst c; typ = tInt }
    )
    | Ethis -> { desc = Ethis; typ = get_va_type_from_env_by_ident env "this" loc }
    | Eaccess (None, x) -> (
        try let (lvl, f) = get_va_info_from_env_by_ident env x loc in 
            { desc = Eaccess ((if lvl = 0 then Some (expr_typing env depth Ethis loc tr) else None), (x, lvl)); typ = type_from_field f } with
        | _ -> expr_typing env depth (Eaccess (Some { edesc = Ethis; eloc = loc }, x)) loc tr
    )
    | Eaccess (Some e, x) -> let e' = expr_typing env depth e.edesc e.eloc tr in 
        let nx = get_field_name_from_type_by_ident env e'.typ x loc in
            { desc = Eaccess (Some e', (nx, 0)); typ = get_field_type_from_type_by_ident env e'.typ nx loc }
    | Eassign ((None, x), e) -> ( let e' = expr_typing env depth e.edesc e.eloc tr in
        try let (lvl, f) = get_va_info_from_env_by_ident env x loc in 
            if lesser_or_equal env e'.typ (type_from_field f) loc then (
                match f with
                | Var _ -> { desc = Eassign ((None, (x, lvl)), e'); typ = tUnit }
                | _ -> raise (Error (loc, "Illegal assignment: " ^ x ^ " can't be modified."))
            ) else
                raise (Error (loc, "Illegal assignment: " ^ str_of_type e'.typ ^ " is not <= " ^ str_of_type (type_from_field f) ^ "."))
        with
        | _ -> expr_typing env depth (Eassign ((Some { edesc = Ethis; eloc = loc }, x), e)) loc tr
    )
    | Eassign ((Some le, x), e) -> ( let le', e' = expr_typing env depth le.edesc le.eloc tr, expr_typing env depth e.edesc e.eloc tr in
        let nx = get_field_name_from_type_by_ident env le'.typ x loc in
        let _, f = get_field_info_from_type_by_ident env le'.typ nx loc in
            if lesser_or_equal env e'.typ (type_from_field f) loc then (
                match f with
                | Var _ -> { desc = Eassign ((Some le', (nx, 0)), e'); typ = tUnit }
                | _ -> raise (Error (loc, "Illegal assignment: " ^ x ^ "can't be modified."))
            ) else
                error_incompatible_types e.eloc e'.typ "" (type_from_field f)
    )
    | Eunop (Uneg, e) -> ( let e' = expr_typing env depth e.edesc e.eloc tr in 
        if e'.typ = tInt then
            { desc = Eunop (Uneg, e'); typ = tInt }
        else
            error_incompatible_types e.eloc e'.typ "" tInt
    )
    | Eunop (Unot, e) -> let e' = expr_typing env depth e.edesc e.eloc tr in
        if e'.typ = tBoolean then { desc = Eunop (Unot, e'); typ = tBoolean } else error_incompatible_types e.eloc e'.typ "" tBoolean
    | Ebinop (bop, e1, e2) -> (let e1', e2' = expr_typing env depth e1.edesc e1.eloc tr, expr_typing env depth e2.edesc e2.eloc tr in
        match bop with
        | Beq | Bne -> 
            if lesser_or_equal env e1'.typ tAnyRef loc then (
                if lesser_or_equal env e2'.typ tAnyRef loc then
                    { desc = Ebinop (bop, e1', e2'); typ = tBoolean }
                else
                    error_incompatible_types e2.eloc e2'.typ "<= " tAnyRef
            ) else
                error_incompatible_types e1.eloc e1'.typ "<= " tAnyRef
        | Beqeq | Bdiff | Bl | Ble | Bg | Bge -> binop_st_typing env bop e1 e1' e2 e2' tInt tBoolean loc
        | Badd | Bsub | Bmul | Bmod | Bdiv -> binop_st_typing env bop e1 e1' e2 e2' tInt tInt loc
        | Band | Bor -> binop_st_typing env bop e1 e1' e2 e2' tBoolean tBoolean loc
    )
    | Eprint e -> let e' = expr_typing env depth e.edesc e.eloc tr in
        if e'.typ = tInt then { desc = Eprint_int e'; typ = tUnit } 
        else if e'.typ = tString then { desc = Eprint_str e'; typ = tUnit }
        else raise (Error (e.eloc, "Argument is of type " ^ str_of_type e'.typ ^ " but it was expected to be of type Int or String."))
    | Eif (e, e1, e2) -> let e', e1', e2' = expr_typing env depth e.edesc e.eloc tr, expr_typing env (depth + 1) e1.edesc e1.eloc tr, 
                                            expr_typing env (depth + 1) e2.edesc e2.eloc tr in
        if e'.typ = tBoolean then 
            { desc = Eif (e', e1', e2'); typ = max env e1'.typ e2'.typ loc }
        else 
            error_incompatible_types e.eloc e'.typ "" tBoolean
    | Ewhile (e, e1) -> let e', e1' = expr_typing env depth e.edesc e.eloc tr, expr_typing env (depth + 1) e1.edesc e1.eloc tr in
        if e'.typ = tBoolean then
            { desc = Ewhile (e', e1'); typ = tUnit }
        else
            error_incompatible_types e.eloc e'.typ "" tBoolean
    | Block l -> block_typing env (depth + 1) l loc tr
    | Ereturn None -> 
        if lesser_or_equal env tUnit tr loc then
            { desc = Ereturn None; typ = tNothing }
        else
            error_incompatible_types loc tUnit "<= " tr
    | Ereturn (Some e) -> let e' = expr_typing env depth e.edesc e.eloc tr in
        if lesser_or_equal env e'.typ tr loc then
            { desc = Ereturn (Some e'); typ = tNothing }
        else
            error_incompatible_types e.eloc  e'.typ "<= " tr
    | Enew (id, tpl, el) -> let cl, ty = get_class_from_env_by_sident env id loc, type_of_tp env (Type (id, tpl)) loc in
        if well_formed env ty loc then (
            { desc = Enew (id, compare_expr_list_with_expected_types env depth el 
                                    (subst_list env cl.input (dummy_list cl) (type_list_of_tpl env tpl loc) loc) loc tr);
              typ = ty }
        ) else
            raise (Error (loc, "Type " ^ str_of_type ty ^ " is not well formed."))
    | Ecall ((None, id), tpl, el) -> expr_typing env depth (Ecall ((Some { edesc = Ethis; eloc = loc }, id), tpl, el)) loc tr
    | Ecall ((Some e, id), tpl, el) -> ( let e' = expr_typing env depth e.edesc e.eloc tr in
        let c = get_class_from_type env e'.typ loc in
        let (dummies, inputs, output) = get_method_from_class_by_ident c id loc in
        let rec go_through tyl dummies = (
            match tyl, dummies with
            | ty :: tyl', did :: dummies' -> ( let d = get_class_from_env_by_ident env did loc in
                match d with
                | { lbl = Dummy (_, _, Greater t') } -> 
                    if well_formed env ty loc then (
                        if lesser_or_equal env (subst env t' (dummy_list c) (type_list_of_tpl env tpl loc) loc) ty loc then
                            go_through tyl' dummies'
                        else 
                            error_incompatible_types loc ty "<= " (subst env t' (dummy_list c) (type_list_of_tpl env tpl loc) loc)
                    ) else
                        raise (Error (loc, "Type " ^ str_of_type ty ^ " is not well formed."))
                | { lbl = Dummy (_, _, Lesser t') } ->
                    if well_formed env ty loc then (
                        if lesser_or_equal env ty (subst env t' (dummy_list c) (type_list_of_tpl env tpl loc) loc) loc then
                            go_through tyl' dummies'
                        else
                            error_incompatible_types loc ty ">= " (subst env t' (dummy_list c) (type_list_of_tpl env tpl loc) loc)
                    ) else
                        raise (Error (loc, "Type " ^ str_of_type ty ^ " is not well formed."))
                | { lbl = Dummy (_, _, No) } ->
                    go_through tyl' dummies'
                | _ -> assert false
            )
            | [], [] -> ()
            | _ -> assert false
        ) in
            go_through (type_list_of_tpl env tpl loc) dummies;
            let lint' = subst_list env inputs (dummy_list c) (get_perm_from_type e'.typ) loc in
            let lint = subst_list env lint' dummies (type_list_of_tpl env tpl loc)  loc in
            let el' = compare_expr_list_with_expected_types env depth el lint loc tr in
                { desc = Ecall ((Some e', (id, 0)), el'); typ = subst env (subst env output (dummy_list c) (get_perm_from_type e'.typ) loc) 
                                                                     dummies (type_list_of_tpl env tpl loc) loc }
    )
    | Vva _ -> raise (Error (loc, "Illegal variable declaration."))
    | Mdef _ -> raise (Error (loc, "Illegal method declaration."))

and compare_expr_list_with_expected_types env depth l1 l2 loc tr =
    match l1, l2 with
    | [], [] -> []
    | e :: l1', t :: l2' -> let e' = expr_typing env depth e.edesc e.eloc tr in
        if lesser_or_equal env e'.typ t loc then
            e' :: (compare_expr_list_with_expected_types env depth l1' l2' loc tr)
        else 
            error_incompatible_types loc e'.typ "<= " t
    | _ -> raise (Error (loc, "Not the good number of inputs."))

and block_typing env depth l loc tr = 
    match l with
    | [] -> { desc = Block []; typ = tUnit }
    | [e] -> (
        match e.edesc with
        | Vva (v, id, None, er) -> let er' = expr_typing env depth er.edesc er.eloc tr in
            let _ = add_va_to_env env v (id, depth) er'.typ loc in
                { desc = Block [{ desc = Vva (v, (id, depth), er'.typ, er'); typ = tUnit }]; typ = tUnit }
        | Vva (v, id, Some t, er) -> let er' = expr_typing env depth er.edesc er.eloc tr in
            let typ = (type_of_tp env t loc) in 
                if well_formed env typ loc then (
                    if lesser_or_equal env er'.typ typ loc then
                        let _ = add_va_to_env env v (id, depth) typ loc in
                        { desc = Block [{ desc = Vva (v, (id, depth), typ, er'); typ = tUnit }]; typ = tUnit }
                    else
                        error_incompatible_types  e.eloc er'.typ "<= " (type_of_tp env t loc)
                ) else
                    raise (Error (loc, "Type " ^  str_of_type typ ^ " is not well formed."))
        | _ -> let e' = expr_typing env depth e.edesc e.eloc tr in { desc = Block [e']; typ = e'.typ }
    )
    | (e :: l') -> ( 
        match e.edesc with
        | Vva (v, id, None, er) -> let er' = expr_typing env depth er.edesc er.eloc tr in
            let lp = block_typing (add_va_to_env env v (id, depth) er'.typ loc) depth l' (List.hd l').eloc tr in (
                match lp.desc with
                | Block l' -> { desc = Block ({ desc = Vva (v, (id, depth), er'.typ, er'); typ = tUnit } :: l'); typ = lp.typ }
                | _ -> assert false
            )
        | Vva (v, id, Some t, er) -> let er' = expr_typing env depth er.edesc er.eloc tr in
            let typ = type_of_tp env t loc in
                if well_formed env typ loc then (
                    if lesser_or_equal env er'.typ typ loc then (
                        let lp = block_typing (add_va_to_env env v (id, depth) typ loc) depth l' (List.hd l').eloc tr in
                            match lp.desc with
                            | Block l' -> { desc = Block ({ desc = Vva (v, (id, depth), typ, er'); typ = tUnit } :: l'); typ = lp.typ }
                            | _ -> assert false
                    ) else
                        error_incompatible_types e.eloc er'.typ "<= " (type_of_tp env t loc)
                ) else 
                    raise (Error (loc, "Type " ^ str_of_type typ ^ " is not well formed."))
        | _ -> let e', lp = expr_typing env depth e.edesc e.eloc tr, block_typing env depth l' (List.hd l').eloc tr in
            match lp.desc with
            | Block l' -> { desc = Block (e' :: l'); typ = lp.typ }
            | _ -> assert false 
    )

let add_class_to_env env cl loc =
    let b = List.fold_left (fun b el -> (str_of_cls el.lbl <> str_of_cls cl.lbl) && b) true env.cls in
        if b then
            { cls = cl :: env.cls;
              restr = env.restr;
              va = env.va }
        else
            raise (Error (loc, "Class " ^ str_of_cls cl.lbl ^ " is already defined."))

let add_restr_to_env env restr =
    { cls = env.cls;
      restr = restr :: env.restr;
      va = env.va }

let inherit_fields_by_type env t loc = 
    let process_field dl tl l f = (
        match f with
        | (id, b, Var ty) -> (id, true, Var (subst env ty dl tl loc)) :: l
        | (id, b, Val ty) -> (id, true, Val (subst env ty dl tl loc)) :: l
        | (id, b, Method (cll, tyl, ty)) -> (id, true, Method (cll, subst_list env tyl dl tl loc, subst env ty dl tl loc)) :: l
        | (id, b, Input ty) -> (id, true, Val (subst env ty dl tl loc)) :: l
    ) in
        match t with
        | TClass (cid, tyl) -> let cl = get_class_from_env_by_ident env cid loc in 
            let dl = dummy_list cl in List.fold_left (process_field dl tyl) [] (List.rev cl.fields)

let rec replace_in_list l ei ef = 
    match l with
    | [] -> assert false
    | e :: l' -> if e = ei then ef :: l' else e :: (replace_in_list l' ei ef)

let replace_class_in_env env cli clf =
    { cls = replace_in_list env.cls cli clf; restr = env.restr; va = env.va }

let rec remove_from_list l x =
     match l with
    | [] -> assert false
    | e :: l' -> if e = x then l' else e :: (remove_from_list l' x) 

let remove_class_from_env env cl = 
    { cls = remove_from_list env.cls cl; restr = env.restr; va = env.va }

let remove_va_from_env env va =
    { cls = env.cls; restr = env.restr; va = remove_from_list env.va va }

let remove_restr_from_env env restr =
    { cls = env.cls; restr = remove_from_list env.restr restr; va = env.va }

let rec type_of_class env c loc =
    match c.lbl with 
    | Nothing | Null | Unit | Int | Boolean | String | AnyVal | AnyRef | Any -> TClass ((str_of_cls c.lbl, Lno), [])
    | Dummy (_, id', _) -> TClass (id', [])
    | Class (id, cl) -> TClass ((id, Lno), List.fold_left (fun l x -> type_of_class env (get_class_from_env_by_ident env x loc) loc :: l) [] (List.rev cl))

let type_list_of_idl env dil loc =
    List.fold_left (fun l x -> type_of_class env (get_class_from_env_by_ident env x loc) loc :: l) [] (List.rev dil)

let check_override_params_and_args env cid dil1 tl1 dil2 tl2 loc =
    let rec go_through l1 l2 = (
        match (l1, l2) with
        | [], [] -> true
        | t1 :: l1', t2 :: l2' -> (equal env t1 t2 loc) && (go_through l1' l2')
        | _ -> assert false
    ) in 
    let cl = get_class_from_env_by_ident env cid loc in
    let text = (match cl.ext with | Some x -> x | _ -> assert false) in
    let TClass (id, stext) = text in
    let dlext = dummy_list (get_class_from_env_by_ident env id loc) in
    let bound_sgn c = (
        match c.lbl with
        | Dummy (_, _, Greater _) -> +1
        | Dummy (_, _, Lesser _) -> -1
        | Dummy (_, _, No) -> 0
        | _ -> assert false
    ) in
    let bound_type c = (
        match c.lbl with
        | Dummy (_, _, Greater t) -> t
        | Dummy (_, _, Lesser t) -> t
        | _ -> assert false
    ) in
    let rec check_bounds l1 l2 = (
        match (l1, l2) with
        | [], [] -> true
        | i1 :: l1', i2 :: l2' -> let c1, c2 = get_class_from_env_by_ident env i1 loc, get_class_from_env_by_ident env i2 loc in
            if bound_sgn c1 = 0 || bound_sgn c2 = 0  || 
                (bound_sgn c1 = bound_sgn c2 && subst env (bound_type c2) dlext stext loc = bound_type c1) then
                check_bounds l1' l2'
            else
                false
        | _ -> assert false
    ) in
        if go_through tl1 (subst_list env tl2 dil2 (type_list_of_idl env dil1 loc) loc) then (
            if check_bounds dil1 dil2 then
                ()
            else
                raise (Error (loc, "Overridden methods should have the same parameters' bounds."))
        ) else
            raise (Error (loc, "Overridden methods should have the same arguments types."))

let add_field_to_class_by_ident env ovr cid (id, b, f) loc =
    let cl = get_class_from_env_by_ident env cid loc in
    let test = List.fold_left (fun t (i, _, _) -> t && (i <> id)) true cl.fields in
        if test then (
            if ovr then
                raise (Error (loc, "Cannot override a non-existent method."))
            else
                 replace_class_in_env env cl { lbl = cl.lbl; fields = (id, b, f) :: cl.fields; input = cl.input; ext = cl.ext }
        ) else (
            let (_, b', f') = List.find (fun (i, _, _) -> i = id) cl.fields in
                if b' = true then (
                    if ovr = false then
                        raise (Error (loc, "Field / Method already exists in a superclass."))
                    else (
                        let (dil', tl', ty') = (match f' with | Method (a, b, c) -> (a, b, c) | _ -> assert false) in 
                        let (dil, tl, ty) = (match f with | Method (a, b, c) -> (a, b, c) | _ -> assert false) in
                            if lesser_or_equal env ty (subst env ty' dil' (type_list_of_idl env dil loc) loc) loc then (
                                check_override_params_and_args env cid dil tl dil' tl' loc;
                                replace_class_in_env env cl 
                                    { lbl = cl.lbl; fields = replace_in_list cl.fields (id, b', f') (id, b, f); input = cl.input; ext = cl.ext }
                            ) else
                                error_incompatible_types loc (type_from_field f) "<= " (type_from_field f')
                    )
                ) else
                    raise (Error (loc, "Field / Method already exists."))
        ) 

let did_of_cls c = 
    match c.lbl with
    | Nothing | Null | Unit | Int | Boolean | String | AnyVal | AnyRef | Any -> (str_of_cls c.lbl, Lno)
    | Dummy (_, id', _) -> id'
    | Class (id, _) -> (id, Lno)

let add_def_left_restr env c loc =
    add_restr_to_env env (did_of_cls c, tNull)

let process_method env cid ovr mid ptl inl tp e loc = 
    let process_param_type (l, env) ( pt : Ast.param_type) = ( 
        match pt with
        | id, None -> let did = (id, Lmethod (mid, Lclass cid)) in 
            let cl = { lbl = Dummy (No, did, No); fields = []; input = []; ext = None } in
                did :: l, add_class_to_env env cl loc
        | id, Some (ord, tp) -> let did = (id, Lmethod (mid, Lclass cid)) in let ty = type_of_tp env tp loc in
            if well_formed env ty loc then 
                match ord with
                | Greater -> let cl = { lbl = Dummy (No, did, Greater ty); fields = []; input = []; ext = None } in
                    test_variance env ty Incr loc; did :: l, add_restr_to_env (add_class_to_env env cl loc) (did, ty)
                | Lesser -> let cl = { lbl = Dummy (No, did, Lesser ty); 
                                       fields = inherit_fields_by_type env ty loc; input = []; ext = Some ty } in
                    test_variance env ty Decr loc; did :: l, add_class_to_env env cl loc
            else
                raise (Error (loc, str_of_type ty ^ " is not well formed.")) 
    ) in
    let rdl, env1 = List.fold_left process_param_type ([], env) ptl in
    let dl = List.rev rdl in 
    let process_inp_elem (l, env) (id, tp) = ( let ty = type_of_tp env tp loc in
        if well_formed env ty loc then (
            test_variance env ty Decr loc;
            ty :: l, add_va_to_env env Val (id, 1) ty loc
        ) else
            raise (Error (loc, str_of_type ty ^ " is not well formed."))
    ) in
    let ril, env2' = List.fold_left process_inp_elem ([], env1) inl in
    let il = List.rev ril in 
    let check_return_tp env = ( let ty = type_of_tp env tp loc in
        if well_formed env ty loc then
            test_variance env ty Incr loc
        else
            raise (Error (loc, str_of_type ty ^ " is not well formed."))
    ) in check_return_tp env2';
    let env2 = add_field_to_class_by_ident env2' ovr (cid, Lno) (mid, false, Method (dl, il, type_of_tp env2' tp loc)) loc in
    let e' = expr_typing env2 1 e.edesc e.eloc (type_of_tp env2 tp loc) in 
        if lesser_or_equal env2 e'.typ (type_of_tp env2 tp loc) loc then (
            let remove_inp_elem env (id, tp) = ( let ty = type_of_tp env tp loc in
                remove_va_from_env env ((id, 1), Val ty)
            ) in
            let env3 = List.fold_left remove_inp_elem env2 inl in 
                env3, { desc = Mdef (mid, List.fold_left (fun l (id, _) -> id :: l) [] (List.rev inl), e'); typ = tUnit }
        ) else
            error_incompatible_types e.eloc e'.typ "<= " (type_of_tp env2 tp loc)

let rec class_declarations env cid l loc =
    match l with
    | [] -> env, []
    | d :: l' ->
        let env', texpr' = (
            match d.edesc with
            | Vva (v, id, None, e) -> let e' = expr_typing env 0 e.edesc e.eloc tNothing in
                test_variance env e'.typ (match v with | Var -> Neut | Val -> Incr) loc;
                add_field_to_class_by_ident env false (cid, Lno) (id, false, (match v with | Var -> Var e'.typ | Val -> Val e'.typ)) d.eloc,
                { desc = Vva (v, (id, 0), e'.typ, e'); typ = tUnit }
            | Vva (v, id, Some tp, e) -> let e' = expr_typing env 0 e.edesc e.eloc tNothing in
                let typ = type_of_tp env tp d.eloc in 
                    if well_formed env typ d.eloc then (
                        test_variance env typ (match v  with | Var -> Neut | Val -> Incr) loc;
                        if lesser_or_equal env e'.typ typ loc then
                            (add_field_to_class_by_ident env false (cid, Lno) (id, false, (match v with | Var -> Var typ | Val -> Val typ)) d.eloc,
                            { desc = Vva (v, (id, 0), typ, e'); typ = tUnit })
                        else
                            error_incompatible_types e.eloc e'.typ "<= " (type_of_tp env tp d.eloc)
                    ) else
                        raise (Error (d.eloc, "Type "  ^ str_of_type typ ^ " is not well formed."))
            | Mdef (ovr, id, ptl, inl, tp, e) -> process_method env cid ovr id ptl inl tp e d.eloc
            | _ -> raise (Error (d.eloc, "Illegal expression."))
        ) in 
        let envf, texprl' = class_declarations env' cid l' loc in
            envf, texpr' :: texprl'

let process_class env (c : Ast.cls_desc) loc =
    let (Cclass (cid, ptcl, inl, ext, body) : Ast.cls_desc) = c in
    let process_param_type_class (l, env) (ptc : Ast.param_type_class) = (
        match ptc with 
        | mon, (id, None) -> let did = (id, Lclass cid) in 
            let cl = { lbl = Dummy (mon, did, No); fields = []; input = []; ext = None } in
                did :: l, add_class_to_env env cl loc
        | mon, (id, Some (ord, tp)) -> let did = (id, Lclass cid) in let ty = type_of_tp env tp loc in
            if well_formed env ty loc then   
                match ord with  
                | Greater -> let cl = { lbl = Dummy (mon, did, Greater ty); fields = []; input = []; ext = None } in
                    test_variance env ty Decr loc; did :: l, add_restr_to_env (add_class_to_env env cl loc) (did, ty)
                | Lesser -> let cl = { lbl = Dummy (mon, did, Lesser ty); fields = inherit_fields_by_type env ty loc; input = []; ext = Some ty } in
                    test_variance env ty Incr loc; did :: l, add_class_to_env env cl loc
            else
                raise (Error (loc, str_of_type ty ^ " is not well formed."))
    ) in
    let rdl, env1 = List.fold_left process_param_type_class ([], env) ptcl in
    let dl = List.rev rdl in 
    let test_extends_type env = (
        match ext with
        | None -> ()
        | Some (tp, _) -> let ty = type_of_tp env tp loc in
            if well_formed env ty loc then (
                test_variance env ty Incr loc;
                let TClass (tid, _) = ty  in let t = get_class_from_env_by_ident env tid loc in
                    match t.lbl with 
                    | Class _ | Dummy _ | AnyRef -> ()
                    | _ -> raise (Error  (loc, "Cannot inherit from " ^ str_of_type ty ^ "."))
            ) else
                raise (Error (loc, str_of_type ty ^ " is not well formed."))
    ) in test_extends_type env1;
    let inherit_fields env = (
        match ext with 
        | None -> []
        | Some (tp, _) -> inherit_fields_by_type env (type_of_tp env tp loc) loc
    ) in
    let extends_type env = (
         match ext with
        | None -> Some tAnyRef
        | Some (tp, _) -> Some (type_of_tp env tp loc)
    ) in
    let claux = { lbl = Class (cid, dl); fields = inherit_fields env1; input = []; ext = extends_type env1 } in
    let env1' = add_def_left_restr (add_class_to_env env1 claux loc) claux loc in
    let input_types env = List.rev (List.fold_left (fun l (_, tp) -> (type_of_tp env tp loc) :: l) [] inl) in
    let inpfie = List.fold_left (fun l (id, tp) -> (id ^ "$" ^ cid, false, Input (type_of_tp env1' tp loc)) :: l) [] (List.rev inl) in
    let cl = { lbl = claux.lbl; fields = inpfie @ claux.fields; input = input_types env1'; ext = claux.ext } in
    let env2 = add_def_left_restr (add_class_to_env env1 cl loc) cl loc in 
    let process_inp_elem env (id, tp) = ( let ty = type_of_tp env tp loc in
        if well_formed env ty loc then
            env 
        else
            raise (Error (loc, str_of_type ty ^ " is not well formed."))
    ) in
    let env3' = List.fold_left process_inp_elem env2 inl in
    let env3 = add_va_to_env env3' Val ("this", 0) (type_of_class env3' cl loc) loc in
    let test_new_extends env = (
        match ext with 
        | None -> None
        | Some (Type (id, at), el) -> Some (expr_typing env 0 (Enew (id, at, el)) loc tAny)
    ) in 
    let finext = test_new_extends env3 in
    let env4, texprl = class_declarations env3 cid body loc in 
    let envf = remove_va_from_env env4 (("this", 0), Val (type_of_class env4 cl loc)) in
        envf, ClassDef (cid, List.fold_left (fun lc (id, _) -> id :: lc ) [] (List.rev inl), finext, texprl)

let init_env () = 
    add_class_to_env (add_class_to_env
    { cls = List.fold_left (fun l t -> { lbl = t; fields = []; input = []; ext = None } :: l) [] 
            [Nothing; Null; Unit; Int; Boolean; String; AnyVal; AnyRef; Any];
      restr = [ (did_of_cls cNull, tNothing); (did_of_cls cString, tNull); (did_of_cls cAnyRef, tString); (did_of_cls cAny, tAnyRef); 
                (did_of_cls cAny, tAnyVal); (did_of_cls cUnit, tNothing); (did_of_cls cInt, tNothing); (did_of_cls cBoolean, tNothing); 
                (did_of_cls cAnyVal, tUnit); (did_of_cls cAnyVal, tInt); (did_of_cls cAnyVal, tBoolean) ];
      va = [] } { lbl = Dummy (Neut, ("S", Lclass "Array"), No); fields = []; input = []; ext = None } (0, 0, 0))
    { lbl = Class ("Array", [("S", Lclass "Array")]); fields = []; input = []; ext = None } (0, 0, 0)

let check_main envf loc = 
    let cl = get_class_from_env_by_ident envf ("Main", Lno) loc in
    let cll, tyl, tr = get_method_from_class_by_ident cl "main" loc in
        if cll <> [] || tyl <> [TClass (("Array", Lno), [tString])] || tr <> tUnit then
            raise (Error (loc, "Objec t Main restrictions aren't met."))

let file (classes, main) =
    let rec go_through env l = (
        match l with
        | [] -> env, []
        | { cdesc = c; cloc = loc } :: l' -> 
            let env', tcls' = process_class env c loc in
            let envf, tclsl = go_through env' l' in
                envf, tcls' :: tclsl
    ) in 
    let main_env, tclsl  = go_through (init_env ()) classes in 
        match main with
        | { odesc = Omain body; oloc = loc } -> 
            let envf, tmain  = process_class main_env (Cclass ("Main", [], [], None, body)) loc in
                check_main envf loc;
                envf, (tclsl, tmain)
