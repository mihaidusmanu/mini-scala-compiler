open Ast
open Tast
open X86_64
open Format

let tUnit = TClass (("Unit", Lno), [])

module StrIntPair = 
    struct
        type t = string * int
        let compare (x0, y0) (x1, y1) =
            match Pervasives.compare x0 x1 with
            | 0 -> Pervasives.compare y0 y1
            | c -> c
    end

module PairsMap = Map.Make(StrIntPair)

let offsets = ref (PairsMap.empty : int PairsMap.t)

let nbStr = ref 0
let nbIf = ref 0 
let nbWhile = ref 0
let nbBinaryOp = ref 0

let regOrder = [|r8; r9; r10; r11; r12; r13; r14|]

let rec init_offsets ofsc e =
    match e.desc with
    | Eunit | Ecst _ | Ethis | Eaccess (None, _) | Ereturn None -> ofsc
    | Eaccess (Some e, _) -> init_offsets ofsc e
    | Eassign ((None, _), e') -> init_offsets ofsc e'
    | Eassign ((Some e, _), e') -> let f1, f2 = init_offsets ofsc e, init_offsets ofsc e' in max f1 f2
    | Ecall ((None, _), _) -> assert false
    | Ecall ((Some e, _), args) -> let f1, f2 = init_offsets ofsc e, List.fold_left (fun ofs e -> max ofs (init_offsets ofsc e)) ofsc args in max f1 f2
    | Enew (_, args) -> List.fold_left (fun ofs e -> max ofs (init_offsets ofsc e)) ofsc args
    | Eunop (_, e) -> init_offsets ofsc e
    | Ebinop (_, e1, e2) -> let f1, f2 = init_offsets ofsc e1, init_offsets ofsc e2 in max f1 f2
    | Eif (e1, e2, e3) -> let f1, f2, f3 = init_offsets ofsc e1, init_offsets ofsc e2, init_offsets ofsc e3 in max f1 (max f2 f3)
    | Ewhile (e1, e2) -> let f1, f2 = init_offsets ofsc e1, init_offsets ofsc e2 in max f1 f2
    | Ereturn (Some e) -> init_offsets ofsc e
    | Eprint_int e -> init_offsets ofsc e
    | Eprint_str e -> init_offsets ofsc e
    | Mdef (_, _, _) -> assert false
    | Vva (_, (id, lbl), _, e) -> let f = init_offsets ofsc e in
        offsets := PairsMap.add (id, lbl) ofsc !offsets;
        max f (ofsc + 8)
    | Block le -> List.fold_left (fun ofs e -> max ofs (init_offsets ofs e)) ofsc le

let string_of_classes_label lbl = 
    match lbl with
    | Nothing -> "Nothing"
    | Null -> "Null"
    | Unit -> "Unit"
    | Int -> "Int"
    | Boolean -> "Boolean"
    | String -> "String" 
    | AnyVal -> "AnyVal"
    | AnyRef -> "AnyRef"
    | Any -> "Any"
    | Class (id, _) -> id
    | Dummy _ -> "" 

let get_class_from_env_by_ident env cid = 
    List.find (fun { lbl = l } -> string_of_classes_label l = cid) env.cls

let super_class_ident cl =
    let ext = cl.ext in
        match ext with
        | None -> "Any"
        | Some (TClass ((id, _), _)) -> id  

let class_of_type env (TClass ((cid, _), _)) =
    get_class_from_env_by_ident env cid

let get_ofs_of_va_by_ident env cl id =
    Pervasives.fst (
        List.fold_left 
            (fun (ans, ofsc) (cid, _, f) -> match f with
                | Var _ | Val _ -> if cid = id then ofsc, (ofsc + 8) else ans, (ofsc + 8)
                | Input _ -> if cid = id then ofsc, (ofsc + 8) else ans, (ofsc + 8)
                | Method _ -> ans, ofsc) 
            (0, 8) 
            (List.rev cl.fields))

let get_ofs_of_meth_by_ident env cl id =
    Pervasives.fst (
        List.fold_left
            (fun (ansc, ofsc) (cid, _, f) -> match f with
                | Var _ | Val _ | Input _ -> ansc, ofsc
                | Method _ -> if cid = id then ofsc, (ofsc + 8) else ansc, (ofsc + 8))
        (0, 8)
        (List.rev cl.fields))

let get_type_of_meth_by_ident env cl id =
    let (_, _, f) = List.find (fun (cid, _, f) -> cid = id) cl.fields in
        match f with
        | Method (_, _, t) -> t
        | _ -> assert false

let class_ident_from_type typ =
    match typ with
    | TClass ((id, _), _) -> id

let rec compile_expr env e = 
    match e.desc with
    | Eunit -> 
        nop, nop
    | Ecst c -> (
        match c with 
        | Cnull -> pushq (imm 0), nop
        | Cbool true -> pushq (imm 1), nop
        | Cbool false -> pushq (imm 0), nop
        | Cstring str -> let rn = ".str_" ^ Pervasives.string_of_int (!nbStr) in 
            incr nbStr;
            pushq (ilab rn), label rn ++ string str
        | Cint i -> pushq (imm i), nop 
    )
    | Ethis -> pushq (reg r15), nop
    | Eaccess (None, (id, lvl)) -> 
        let ofs = -(PairsMap.find (id, lvl) !offsets) in
            pushq (ind ~ofs rbp), nop
    | Eaccess (Some e, (id, lvl)) -> let c, d = compile_expr env e in
        let ofs = get_ofs_of_va_by_ident env (class_of_type env e.typ) id in
            c ++ popq rax ++ pushq (ind ~ofs rax), d 
    | Eassign ((None, (id, lvl)), e) -> 
        let ofs = -(PairsMap.find (id, lvl) !offsets) in 
        let c, d = compile_expr env e in 
            c ++ (if e.typ = tUnit then pushq (imm 0) else nop) ++ popq rax ++ movq (reg rax) (ind ~ofs rbp), d
    | Eassign ((Some e1, (id, lvl)), e) -> let c1, d1 = compile_expr env e1 in let c, d = compile_expr env e in 
        let ofs = get_ofs_of_va_by_ident env (class_of_type env e1.typ) id in
            c1 ++ c ++ popq rbx ++ popq rax ++ (if e.typ = tUnit then pushq (imm 0) else nop) ++ movq (reg rbx) (ind ~ofs rax), d1 ++ d
    | Ecall ((None, (id, lvl)), args) -> assert false
    | Ecall ((Some e1, (id, lvl)), args) -> let c1, d1 = compile_expr env e1 in 
        let class_e1 = class_of_type env e1.typ in
        let c, d, _ = List.fold_left
            (fun (c, d, p) e -> let c', d' = compile_expr env e in c ++ c', d ++ d', p + 1)
            (nop, nop, 0)
            args in
        let c', _ = List.fold_left
            (fun (c, p) _ -> popq regOrder.(p) ++ c, p + 1)
            (nop, 0)
            args in
            c1 ++ c ++ c' ++ popq rax ++ pushq (reg r15) ++ movq (reg rax) (reg r15) ++ movq (ind ~ofs:0 rax) (reg rsi) ++
            call_star (ind ~ofs:(get_ofs_of_meth_by_ident env class_e1 id) rsi) ++ 
            popq rbx ++ movq (reg rbx) (reg r15) ++
            (if get_type_of_meth_by_ident env class_e1 id = tUnit then nop else pushq (reg rax)), d1 ++ d
    | Enew (cid, args) -> 
        let c, d, len = List.fold_left 
            (fun (c, d, p) e -> let c', d' = compile_expr env e in c ++ c', d ++ d', p + 1) 
            (nop, nop, 0) 
            args in
        let c', _ = List.fold_left
            (fun (c, p) e -> c ++ popq regOrder.(p), p + 1)
            (nop, 0)
            args in
            c ++
            movq 
                (imm (List.fold_left (fun x (_, _, f) -> match f with | Var _ | Val _ | Input _ -> x + 8 | _ -> x) 8 (get_class_from_env_by_ident env cid).fields))
                (reg rdi) ++ call "malloc" ++ movq (reg rax) (reg rdi) ++ c' ++
            movq (ilab ("D_" ^ cid)) (reg rax) ++ movq (reg rax) (ind ~ofs:0 rdi) ++ 
            call ("C_" ^ cid) ++ pushq (reg rdi), d 
    | Eunop (o, e) -> let c, d = compile_expr env e in
        c ++
        ( match o with
        | Uneg ->
            popq rbx ++
            movq (imm 0) (reg rax) ++
            subq (reg rbx) (reg rax)
        | Unot -> 
            popq rax ++
            notq (reg rax) ++
            andq (imm 1) (reg rax)) ++
        pushq (reg rax), d
    | Ebinop (o, e1, e2) -> let c1, d1 = compile_expr env e1 in let c2, d2 = compile_expr env e2 in 
        (match o with | Band | Bor -> incr nbBinaryOp | _ -> ());
        c1 ++ 
        ( match o with 
        | Band -> 
            popq rax ++ 
            movq (imm 1) (reg rcx) ++ andq (reg rcx) (reg rax) ++ 
            je ("BinaryOp_" ^ Pervasives.string_of_int (!nbBinaryOp - 1) ^ "_False") ++
            jmp ("BinaryOp_" ^  Pervasives.string_of_int (!nbBinaryOp - 1) ^ "_True") ++
            label ("BinaryOp_" ^ Pervasives.string_of_int (!nbBinaryOp - 1) ^ "_False") ++
            movq (imm 0) (reg rax) ++ jmp ("BinaryOp_" ^ Pervasives.string_of_int (!nbBinaryOp - 1) ^ "_End") ++
            label ("BinaryOp_" ^ Pervasives.string_of_int (!nbBinaryOp - 1) ^ "_True") ++
            pushq (reg rax) ++ 
	        c2 ++
            popq rbx ++ popq rax ++ movq (imm 1) (reg rcx) ++ andq (reg rbx) (reg rax) ++
            jmp ("BinaryOp_" ^ Pervasives.string_of_int (!nbBinaryOp - 1) ^ "_End") ++
            label ("BinaryOp_" ^ Pervasives.string_of_int (!nbBinaryOp - 1) ^ "_End")
        | Bor -> 
	        popq rax ++ 
            movq (imm 0) (reg rcx) ++ orq (reg rcx) (reg rax) ++ 
            je ("BinaryOp_" ^ Pervasives.string_of_int (!nbBinaryOp - 1) ^ "_False") ++
            jmp ("BinaryOp_" ^  Pervasives.string_of_int (!nbBinaryOp - 1) ^ "_True") ++
            label ("BinaryOp_" ^ Pervasives.string_of_int (!nbBinaryOp - 1) ^ "_True") ++
            movq (imm 1) (reg rax) ++ jmp ("BinaryOp_" ^ Pervasives.string_of_int (!nbBinaryOp - 1) ^ "_End") ++
            label ("BinaryOp_" ^ Pervasives.string_of_int (!nbBinaryOp - 1) ^ "_False") ++
            pushq (reg rax) ++
	        c2 ++
            popq rbx ++ popq rax ++ movq (imm 1) (reg rcx) ++ orq (reg rbx) (reg rax) ++
            jmp ("BinaryOp_" ^ Pervasives.string_of_int (!nbBinaryOp - 1) ^ "_End") ++
            label ("BinaryOp_" ^ Pervasives.string_of_int (!nbBinaryOp - 1) ^ "_End")
        | _ -> (
            c2 ++
            popq rbx ++ popq rax ++ movq (imm 1) (reg rcx) ++ 
            ( match o with
            | Badd -> addq (reg rbx) (reg rax)
            | Bsub -> subq (reg rbx) (reg rax)
            | Bmul -> imulq (reg rbx) (reg rax)
            | Bmod -> movq (imm 0) (reg rdx) ++ cqto ++ idivq (reg rbx) ++ movq (reg rdx) (reg rax)
            | Bdiv -> movq (imm 0) (reg rdx) ++ cqto ++ idivq (reg rbx)
            | Beq -> xorq (reg rdx) (reg rdx) ++ cmpq (reg rbx) (reg rax) ++ cmove (reg rcx) (reg rdx) ++ movq (reg rdx) (reg rax)
            | Bne -> xorq (reg rdx) (reg rdx) ++ cmpq (reg rbx) (reg rax) ++ cmovne (reg rcx) (reg rdx) ++ movq (reg rdx) (reg rax)
            | Beqeq -> xorq (reg rdx) (reg rdx) ++ cmpq (reg rbx) (reg rax) ++ cmove (reg rcx) (reg rdx) ++ movq (reg rdx) (reg rax)
            | Bdiff -> xorq (reg rdx) (reg rdx) ++ cmpq (reg rbx) (reg rax) ++ cmovne (reg rcx) (reg rdx) ++ movq (reg rdx) (reg rax)
            | Bg -> xorq (reg rdx) (reg rdx) ++ cmpq (reg rbx) (reg rax) ++ cmovg (reg rcx) (reg rdx) ++ movq (reg rdx) (reg rax)
            | Bge -> xorq (reg rdx) (reg rdx) ++ cmpq (reg rbx) (reg rax) ++ cmovge (reg rcx) (reg rdx) ++ movq (reg rdx) (reg rax)
            | Bl -> xorq (reg rdx) (reg rdx) ++ cmpq (reg rbx) (reg rax) ++ cmovl (reg rcx) (reg rdx) ++ movq (reg rdx) (reg rax)
            | Ble -> xorq (reg rdx) (reg rdx) ++ cmpq (reg rbx) (reg rax) ++ cmovle (reg rcx) (reg rdx) ++ movq (reg rdx) (reg rax)
            | _ -> assert false))) ++
        pushq (reg rax), d1 ++ d2
    | Eif (e1, e2, e3) -> 
        let lblTrue = "If_"  ^ Pervasives.string_of_int !nbIf ^ "_True" in
        let lblFalse = "If_" ^ Pervasives.string_of_int !nbIf ^ "_False" in
        let lblEnd = "If_" ^ Pervasives.string_of_int !nbIf ^ "_End" in incr nbIf;
        let c1, d1 = compile_expr env e1 in let c2, d2 = compile_expr env e2 in let c3, d3 = compile_expr env e3 in
            c1 ++ popq rax ++ testq (reg rax) (reg rax) ++ 
            jne lblTrue ++ jmp lblFalse ++
            label lblTrue ++ c2 ++ jmp lblEnd ++
            label lblFalse ++ c3 ++ jmp lblEnd ++
            label lblEnd, d1 ++ d2 ++ d3
    | Ewhile (e1, e2) -> 
        let lblCond = "While" ^ Pervasives.string_of_int !nbWhile ^ "_Cond" in
        let lblBody = "While" ^ Pervasives.string_of_int !nbWhile ^ "_Body" in
        let lblEnd = "While" ^ Pervasives.string_of_int !nbWhile ^ "_End" in incr nbWhile;
        let c1, d1 = compile_expr env e1 in let c2, d2 = compile_expr env e2 in
            jmp lblCond ++
            label lblCond ++ c1 ++ popq rax ++ testq (reg rax) (reg rax) ++
            jne lblBody ++ jmp lblEnd ++
            label lblBody ++ c2 ++ jmp lblCond ++
            label lblEnd, d1 ++ d2
    | Ereturn None -> 
        movq (imm 0) (reg rax) ++ movq (reg rbp) (reg rsp) ++ popq rbp ++ ret, nop
    | Ereturn (Some e) -> let c, d = compile_expr env e in
        c ++ popq rax ++ movq (reg rbp) (reg rsp) ++ popq rbp ++ ret, d
    | Eprint_int e -> let c, d = compile_expr env e in
        c ++
        popq rdi ++
        call "print_int", d
    | Eprint_str e ->  let c, d = compile_expr env e in
        c ++
        popq rdi ++
        call "print_str", d
    | Block el ->
        List.fold_left (fun (ce, d) e -> let ce', d' = compile_expr env e in ce ++ ce', d ++ d')  (nop, nop) el
    | Mdef (_, _, _) -> assert false
    | Vva (_, (id, lvl), _, e) -> let ce, d = compile_expr env e in
        let ofs = -(PairsMap.find (id, lvl) !offsets) in 
            ce ++ popq rax ++ movq (reg rax) (ind ~ofs rbp), d

let find_field_declaration_from_class cl fid =
    List.find (fun (id, b, f) -> fid = id) cl.fields

let find_class_body env clsl cl = 
    let cid = string_of_classes_label cl.lbl in
    let (ClassDef (_, _, _, b)) = List.find (fun (ClassDef (id, _, _, _)) -> id = cid) clsl in
        b  

let find_field_body_by_ident body fid =
    let e = List.find 
        (fun e -> match e.desc with
            | Vva (_, (id, _), _, _) -> id = fid
            | Mdef (id, _, _) -> id = fid
            | _ -> assert false)
        body in
        match e.desc with
        | Vva  (_, _, _, b) -> b
        | Mdef (_, _, b) -> b
        | _ -> assert false

let rec find_field_body env clsl cl (id, b, f) =
    if b then (
        let newcl = get_class_from_env_by_ident env (super_class_ident cl) in
        let newf = find_field_declaration_from_class newcl id in
            find_field_body env clsl newcl newf
    ) else
        let clb = find_class_body env clsl cl in
            find_field_body_by_ident clb id

let create_constr env clsl cl cid inl el ext = 
    let extends_smth, (ec, ed) = ( 
        match ext with
        | None -> false, (nop, nop)
        | Some e -> true, compile_expr env e) in
    let extends_cl = get_class_from_env_by_ident env 
        (match ext with 
        | None -> "Any" 
        | Some ({desc = Enew(id, _)}) -> id 
        | _ -> assert false) in
    let (cinp, _, _) =
        List.fold_left
            (fun (c, ofs, pc) fc -> match fc with
                | (id, _, Var _) | (id, _, Val _) -> c, ofs + 8, pc
                | _ , _, Input _ -> c ++ movq (reg regOrder.(pc)) (ind ~ofs rdi), ofs + 8, pc + 1
                | _ -> c, ofs, pc)
            (nop, 8, 0)
            (List.rev cl.fields) in
    let (c, d, _, _) = 
        List.fold_left 
            (fun (c, d, ofs, pc) fc -> match fc with 
                | (id, false, Var _) | (id, false, Val _) -> let e = find_field_body env clsl cl fc in
                    let c', d' = compile_expr env e in 
                        c ++ pushq (reg rdi) ++ c' ++ 
                        (if e.typ = tUnit then pushq (imm 0) else nop) ++ 
                        popq rax ++ popq rdi ++ movq (reg rax) (ind ~ofs rdi), d ++ d', ofs + 8, pc
                | (id, true, Var _) | (id, true, Val _) -> let scofs = get_ofs_of_va_by_ident env extends_cl id in
                    c ++ movq (ind ~ofs:scofs rbx) (reg rax) ++ movq (reg rax) (ind ~ofs rdi), d, ofs + 8, pc
                | _, _, Input _ -> c, d, ofs + 8, pc + 1
                | _ -> c, d, ofs, pc)
            (nop, nop, 8, 0) 
            (List.rev cl.fields) in 
        label ("C_" ^ cid)  ++
        pushq (reg r15) ++ movq (reg rdi) (reg r15) ++ 
        cinp ++ 
        (if extends_smth then 
            pushq (reg r15) ++ pushq (reg rdi) ++ 
            ec ++ popq rbx ++ 
            popq rdi ++ popq rax ++ movq (reg rax) (reg r15) 
        else nop) ++
        c ++ popq rax ++ movq (reg rax) (reg r15) ++ ret, ed ++ d
    
let find_method_declaration_by_ident mid el = 
    List.find (fun { desc = d } -> match d with | Mdef (id, _, _) -> id = mid | _ -> false) el

let find_method_body_by_ident mid el =
    match (find_method_declaration_by_ident mid el).desc with
    | Mdef (_, _, el) -> el
    | _ -> assert false

let find_method_inl_by_ident mid el =
    match (find_method_declaration_by_ident mid el).desc with
    | Mdef (_, inl, _) -> inl
    | _ -> assert false

let create_meth env clsl cl cid inl el =
    List.fold_left 
        (fun (c, d) f -> 
            match f with
            | mid, true, Method (_, _, _) -> c ++ label ("M_" ^ cid ^ "$" ^ mid) ++ jmp ("M_" ^ super_class_ident cl ^ "$" ^ mid), d
            | mid, false, Method (_, _, tc) -> let inp = find_method_inl_by_ident mid el in offsets := PairsMap.empty;
                let nbc = List.fold_left (fun ofsc id -> offsets := PairsMap.add (id, 1) ofsc !offsets; ofsc + 8 ) 8 inp in
                let cin, din, _ = List.fold_left 
                    (fun (c, d, ord) id -> let ofs = -(PairsMap.find (id, 1) !offsets) in
                        c ++ movq (reg regOrder.(ord)) (ind ~ofs rbp), d, ord + 1)
                    (nop, nop, 0) 
                    inp in
                let mb = find_method_body_by_ident mid el in
                let nb = init_offsets nbc mb in
                let ce, de = compile_expr env mb in
                    c ++ 
                    label ("M_" ^ cid ^ "$" ^ mid) ++ pushq (reg rbp) ++ movq (reg rsp) (reg rbp) ++ 
                    subq (imm nb) (reg rsp) ++
                    cin ++ ce ++ (if tc = tUnit then pushq (imm 0) else nop) ++ popq rax ++
                    movq (reg rbp) (reg rsp) ++ popq rbp ++ ret, d ++ din ++ de
            | _ -> c, d) 
        (nop, nop)
        cl.fields

let create_descr env clsl cl cid inl el =
    label ("D_" ^ cid) ++ 
    address ( 
        ("D_" ^ super_class_ident cl) ::
        List.fold_left
            (fun l c ->
                match c with
                | mid, _, Method (_, _, _) -> ("M_" ^ cid ^ "$" ^ mid) :: l
                | _ -> l)
            []
            cl.fields), 
    nop

let process_class env clsl (ClassDef (cid, inl, ext, el)) =
    let cl = get_class_from_env_by_ident env cid in
    let constr, dc = create_constr env clsl cl cid inl el ext in
    let meth, dm = create_meth env clsl cl cid inl el in
    let descr, dd = create_descr env clsl cl cid inl el in
        constr, meth, descr, dc ++ dm ++ dd

let process_program env (clsl, main) = 
    let clslf = clsl @ [main] in
    let cconstr, cmeth, cdescr, cdata = List.fold_left 
        (fun (constr, meth, descr, data) cls -> let nconstr, nmeth, ndescr, ndata = process_class env clslf cls in 
            constr ++ nconstr, meth ++ nmeth, descr ++ ndescr, data ++ ndata) 
        (nop, nop, nop, nop) 
        clsl in 
    let mconstr, mmeth, mdescr, mdata = process_class env clslf main in
        cconstr ++ mconstr, cmeth ++ mmeth, cdescr ++ mdescr, cdata ++ mdata

let tAny = TClass (("Any", Lno), []) 

let compile_program env prog ofile = 
    let constr, meth, descr, data = process_program env prog in
    let p = {
        text =   
            glabel "main" ++
            movq 
                (imm (List.fold_left 
                    (fun x (_, _, f) -> match f with | Var _ | Val _ | Input _ -> x + 8 | _ -> x) 8 (get_class_from_env_by_ident env "Main").fields))
                (reg rdi) ++ 
            call "malloc" ++ movq (reg rax) (reg rdi) ++
	        movq (ilab ("D_Main")) (reg rax) ++ movq (reg rax) (ind ~ofs:0 rdi) ++ 
            call "C_Main" ++ movq (reg rdi) (reg r15) ++
            call "M_Main$main" ++
            xorq (reg rax) (reg rax) ++
            ret ++
            label "print_int" ++
            movq (reg rdi) (reg rsi) ++
            movq (ilab ".Sprint_int") (reg rdi) ++
            movq (imm 0) (reg rax) ++
            call "printf" ++
            ret ++
            label "print_str" ++
            movq (reg rdi) (reg rsi) ++
            movq (ilab ".Sprint_str") (reg rdi) ++
            movq (imm 0) (reg rax) ++
            call "printf" ++
            ret ++ 
            constr ++
            meth;
        data = 
            (label ".Sprint_int" ++ string "%d") ++  
            (label ".Sprint_str" ++ string "%s") ++ (label "D_Any") ++ (label "D_AnyRef") ++ (address ["D_Any"]) ++ 
            descr ++ data
    } in
    let f = open_out ofile in
    let fmt = formatter_of_out_channel f in
        X86_64.print_program fmt p;
        fprintf fmt "@?"; close_out f
