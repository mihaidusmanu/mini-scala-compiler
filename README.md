# **Mini-Scala compiler** #

Mini-Scala compiler written in OCaml (using OCamllex and Menhir) for Compilation and Programming Languages course (Ecole Normale Superieure, Paris).

Project description can be found here : https://www.lri.fr/~filliatr/ens/compil/ .

| Feature          | Status   |
|------------------|:--------:|
| Lexer            | ✔        |
| Parser           | ✔        |
| Typing algorithm | ✔        |
| Code generator   | ✔        |

---
## *Compilation* ##

`make`

If you want to delete the files created by `make`, you can use:

`make clean`

## *Execution example* ##

`./pscala file.scala`

---
## *Options* ##

`./pscala [--parse-only] [--type-only] file.scala`

- `--parse-only`: Runs only the first 2 stages of compilation (Lexing & Parsing).
- `--type-only`: Runs only the first 3 stages of compilation (Lexing, Parsing & Typing).
