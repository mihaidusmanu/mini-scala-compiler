open Ast

type ident = string

type level = int

type smart_ident = ident * level

type link = 
    | Lclass of ident 
    | Lmethod of ident * link
    | Lno

type dummy_ident = ident * link

type order = 
    | Greater of types
    | Lesser of types
    | No

and classes_label = 
    | Nothing | Null | Unit | Int | Boolean | String | AnyVal | AnyRef | Any
    | Class of ident * (dummy_ident list)
    | Dummy of monotony * dummy_ident * order

and classes = { lbl : classes_label; fields : (ident * bool * field) list; input : (types list); ext : types option }

and types =
    | TClass of dummy_ident * (types list)

and field = 
    | Var of types
    | Val of types
    | Method of (dummy_ident list) * (types list) * types
    | Input of types

and env = { cls : classes list; restr : ((dummy_ident * types) list); va : ((smart_ident * field) list) }

and texpr = {
    desc : tdesc;
    typ : types
} and tdesc = 
(* expr *)
    | Eunit
    | Ecst of constant
    | Ethis
    | Eaccess of access
    | Eassign of access * texpr
    | Ecall of access * (texpr list)
    | Enew of ident * (texpr list)
    | Eunop of unop * texpr
    | Ebinop of binop * texpr * texpr
    | Eif of texpr * texpr * texpr
    | Ewhile of texpr * texpr
    | Ereturn of (texpr option)
    | Eprint_int of texpr
    | Eprint_str of texpr
(* var *)
    | Vva of va_ * smart_ident * types * texpr
(* method *)
    | Mdef of ident * (ident list) * texpr
(* block *)
    | Block of (texpr list)

and tcls = 
    | ClassDef of ident * (ident list) * (texpr option) * (texpr list)

and access = (texpr option) * smart_ident

type tfile = (tcls list) * tcls
