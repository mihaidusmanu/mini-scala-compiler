open Lexing
open Format
open Parser
open Typing

exception Suffix_error

let usage = "Usage: pscala [options] file.pscala"

let parse_only = ref false
let type_only = ref false

let spec = ["--parse-only", Arg.Set parse_only, "  stop after parsing"; "--type-only", Arg.Set type_only, " stop after typing"]

let file =
    let file = ref None in
    let set_file s =
        if not (Filename.check_suffix s ".scala") then
            raise (Arg.Bad "no .scala extension");
        file := Some s
    in
        Arg.parse spec set_file usage;
        match !file with Some f -> f | None -> Arg.usage spec usage; exit 1

let report (b, e) =
    let l = b.pos_lnum in
    let fc = b.pos_cnum - b.pos_bol + 1 in
    let lc = e.pos_cnum - b.pos_bol + 1 in
    eprintf "File \"%s\", line %d, characters %d-%d:\n" file l fc lc

let report_pos (l, c1, c2) =
    eprintf "File \"%s\", line %d, characters %d-%d:\n" file l c1 c2

let () =
    let c = open_in file in 
    let lb = Lexing.from_channel c in
    try
        let f = Parser.prog Lexer.token lb in
            close_in c;
            if !parse_only then exit 0;
            let env, tf = Typing.file f in
                if !type_only then exit 0;
                if not (Filename.check_suffix file ".scala") then raise Suffix_error;
                let ofile = Filename.chop_suffix file ".scala" ^ ".s" in
                    Compile.compile_program env tf ofile
    with
    | Lexer.Lexing_error s ->
	    report (lexeme_start_p lb, lexeme_end_p lb);
	    eprintf "Lexical error: %s@." s;
	    exit 1
    | Parser.Error ->
	    report (lexeme_start_p lb, lexeme_end_p lb);
	    eprintf "Syntax error@.";
	    exit 1
    | Typing.Error (p, s) ->
        report_pos p;
        eprintf "Typing error: %s@." s;
        exit 1
    | Suffix_error ->
        report_pos (0, 0, 0);
        eprintf "The input file doesn't have .scala suffix.";
        exit 1
    | e ->
	    eprintf "Anomaly: %s\n@." (Printexc.to_string e);
	    exit 2
